# README

Site disponible à l'adresse https://duniter.org/fr

## Développer localement

Vous aurez besoin des paquets python3, npm, plantuml graphviz

```bash
sudo apt install python3 npm plantuml graphviz
```

Créez ensuite un evnironnement virtuel python dédié à ce site.

```bash
python3 -m venv env         # créer l'environnement "env"
source env/bin/activate     # activer l'environnement
pip install -r requirements.txt # installer les dépendances python
```

Compilez le site et servez-le localement 

```bash
pelican content
pelican --autoreload --listen
```

## Autre

### Plantuml plugin documentation

 * Plantuml plugin documentation : https://github.com/Scheirle/pelican-plugins/tree/master/plantuml
 * Plantuml documentation: http://plantuml.com
 * Plantuml support DOT language of GraphViz: http://www.graphviz.org/Gallery.php

### Ressources CSS utiles

* [Documentation de l'extension Markdown qui gère la génération de sommaires](https://python-markdown.github.io/extensions/toc/)
* [Flexbox : comprendre le dimensionnement des images](https://codepen.io/dudleystorey/pen/pejpYW)

