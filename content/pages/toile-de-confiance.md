Title: La Toile de confiance : un système d'identification pair-à-pair
Breadcrumb: La Toile de confiance

# La Toile de Confiance

<iframe width="560" height="315" src="https://www.youtube.com/embed/xCsjIdeHPJc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

*   [Fonctionnement de la Toile de confiance]({filename}toile-de-confiance/introduction-a-la-toile-de-confiance.md)
*   [La Toile de confiance en détail]({filename}toile-de-confiance/la-toile-de-confiance-en-detail.md)
*   [Étude de la WoT sur le forum Duniter](https://forum.duniter.org/t/etude-de-la-wot/977)
*   [Comment certifier de nouveaux membres]({filename}toile-de-confiance/certifier-de-nouveaux-membres.md)
*   [Questions fréquentes sur la Toile de confiance]({filename}toile-de-confiance/faq-tdc.md)
